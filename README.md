# README #


* This repo just builds a basic docker image file from .NET core source files

### How do I get set up? ###

* Get the repo, of course
* run docker like this: docker build --no-cache -t your_repo/whatever_image_name .     (ie docker build --no-cache -t stefan/netcoretest:1.0 .)
* The previous step will generate a new local docker image.  You can run it interactively with:  run -it your_docker_image_id   (this id is generated from previous step, you can run docker image to verify)


IMPORTANT THINGS TO NOTE:

1.Right now, the .net core app runs a server in port 6060, but the app itself is not working properly, so calling your container with a port will not return anything but a 402 - not found header
2.There is a bunch of source code but the only service I am trying to pack here is named AuthService.  You can check the reference to it under the Dockerfile.
3.The Dockerfile is built to run an app build on asp.net core and hence the line FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS final  (for a regular .net core app, you would use: FROM mcr.microsoft.com/dotnet/core/runtime:3.1)



